import logging
import helpers, os
from datetime import datetime
from constants import CONFIG_WORKING_DIRECTORY, CONFIG_INPUT_DIRECTORY, CONFIG_OUTPUT_DIRECTORY

now = datetime.now()
date_time = now.strftime("%Y%m%dT%H%M%S")

database_file = f"{CONFIG_WORKING_DIRECTORY}database.csv"
# search_dir = r"C:\Users\DC_sp\Mega\Photos"
search_dir = f"{CONFIG_WORKING_DIRECTORY}{CONFIG_INPUT_DIRECTORY}"
duplicate_dir = f"{CONFIG_WORKING_DIRECTORY}Duplicate"
sorted_folder = f"{CONFIG_WORKING_DIRECTORY}{CONFIG_OUTPUT_DIRECTORY}"
log_file = os.path.join(CONFIG_WORKING_DIRECTORY, f"photo_sorter-{date_time}.log")
basedir = os.path.dirname(log_file)

log_format = "%(asctime)s - %(filename)15s:%(lineno)-5s - %(funcName)20s() - %(levelname)-6s -> %(message)s "

# create logger with 'spam_application'
log = logging.getLogger()
log.setLevel(logging.INFO)
# create file handler which logs even debug messages
fh = logging.FileHandler(log_file)
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
# create formatter and add it to the handlers
formatter = logging.Formatter(log_format)
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
log.addHandler(fh)
log.addHandler(ch)
# Sub logging - https://docs.python.org/3/howto/logging-cookbook.html
log.info('Logging started: {}'.format(log_file))
log.info('Root dir: {}'.format(CONFIG_WORKING_DIRECTORY))



db = helpers.Database(database_file)

try:
    #helpers.load_files_to_db(db, search_dir)
    #helpers.load_files_to_db(db, sorted_folder)
    helpers.check_db_for_duplicates_and_move(db, duplicate_dir)
    helpers.move_duplicates(search_dir, duplicate_dir)
    helpers.extract_heic(search_dir)
    helpers.sort_files(search_dir, sorted_folder)

except KeyboardInterrupt:
    log.info("Keyboard stop")
finally:
    helpers.move_duplicates(search_dir, duplicate_dir)
    db.save_db()

