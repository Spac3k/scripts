import datetime
import re, os, configparser
from time import sleep
from os import startfile

import tkinter as tk
from tkinter import ttk, filedialog
import sentry_sdk


sentry_sdk.init(
    dsn="https://5cdcd41da63b026971474f50c7737943@o4505649532436480.ingest.sentry.io/4505773826048000",
    traces_sample_rate=1.0
)

widgets_added = []
config_file_name = "settings.ini"
default_config = {'SETTINGS':{'input_file':'','valid_chars':'valid_chars','auto_bug_report':'Yes'}}
config = configparser.ConfigParser()

def run_program():
    
    output_file_handler = open('results.txt', 'a')
    
    time_at_run = str(datetime.datetime.now().isoformat())
    input_file_name = input_field.get()
    valid_chars = chars_field.get()

    immedaite_write("="*20, output_file_handler)
    immedaite_write(f'time_at_run: {time_at_run}', output_file_handler)
    immedaite_write(f'input_file_name: {input_file_name}', output_file_handler)
    immedaite_write(f'valid_chars: {valid_chars}', output_file_handler)
    immedaite_write("="*20, output_file_handler)

    input_file_handler = open(input_file_name, "r")
  
    detroy_visable()

    progress_label = tk.Label(mid_frame, text="Progress % ")
    progress_label.grid(row=0, column=0, padx=1, pady=2)
    widgets_added.append(progress_label)
    progress = ttk.Progressbar(mid_frame, orient="horizontal", length=200, mode='determinate')
    progress.grid(row=0, column=1, padx=1, pady=2)
    widgets_added.append(progress)

    lines = input_file_handler.readlines()
    total_lines = len(lines)
    lines_processed = 0

    for line in lines:
        for idx, char in enumerate(line):
            if not re.match(valid_chars, char):
                msg = f'Invalid: line_number: {lines_processed} | position: {idx} | char: {char} | Full string: {line}'
                immedaite_write(msg, output_file_handler)
      
        lines_processed += 1
        progress_val = lines_processed/total_lines*100
        progress['value'] = progress_val
        mid_frame.update_idletasks()

    immedaite_write("="*20, output_file_handler)

    input_file_handler.close()
    output_file_handler.close()
    
    program_done()

def immedaite_write(text, file_handler):
    file_handler.write(f'{text}\n')
    file_handler.flush()
    os.fsync(file_handler.fileno())

def program_done():
    detroy_visable()
    done_label = tk.Label(mid_frame, text="Program done, see log file for output.")
    done_label.grid(row=0, column=1, padx=1, pady=2)
    widgets_added.append(done_label)

    results_button = tk.Button(bot_frame, text="Open results", command=open_results)
    results_button.grid(row=0, column=0, padx=1, pady=2)
    widgets_added.append(results_button)

    close_button = tk.Button(bot_frame, text="Close", command=exit)
    close_button.grid(row=0, column=1, padx=1, pady=2)
    widgets_added.append(close_button)

def open_results():
    startfile( 'results.txt' )
    exit()

def detroy_visable():
    global widgets_added
    for widget in widgets_added:
        widget.destroy()
    # print(f'window cleared')

def browse_file():
    filename = filedialog.askopenfilename(initialdir="/", title="Select File",
                                          filetypes=(("Text files", "*.txt"), ("all files", "*.*")))
    input_field.insert(tk.END, filename)

def set_valid_chars(*args):
    selected = valid_chars_var.get()
    if selected == 'Lowercase':
        chars_field.delete(0, tk.END)
        chars_field.insert(tk.END, '[a-z]')
    elif selected == 'AlphaNumeric':
        chars_field.delete(0, tk.END)
        chars_field.insert(tk.END, '[a-zA-Z0-9]')
    elif selected == 'Custom':
        chars_field.delete(0, tk.END)
        chars_field.insert(tk.END, read_setting('SETTINGS','valid_chars_dropdown_custom'))


def update_config(section, key, value):
    global config
    if not config.has_section(section):
        config.add_section(section)
    config.set(section, key, value)
    with open(config_file_name, "w") as confUpdate:
        config.write(confUpdate)
        config.flush()
        os.fsync(config.fileno())

def save_settings():
    global config
    
    config['SETTINGS']['input_file'] = input_field.get()
    config['SETTINGS']['valid_chars'] = chars_field.get()
    config['SETTINGS']['valid_chars_dropdown'] = valid_chars_dropdown.get()
    if config['SETTINGS']['valid_chars_dropdown'] in 'Custom':
        config['SETTINGS']['valid_chars_dropdown_custom'] = config['SETTINGS']['valid_chars']

    with open(config_file_name, "w") as configfile_handler: 
        config.write(configfile_handler)
        print(f'Config updated.')

def load_settings():
    global config
    try:
        test_file = open(config_file_name, "r") # this triggers the FileNotFoundError
        test_file.close()
        config.read(config_file_name)
        print(f'Config file read from : {config_file_name}')
    except FileNotFoundError:
        with open(config_file_name, "w") as configfile_handler: 
            config.write(configfile_handler)
        print('Created config file')

    if len(config.sections()) == 0:
        config['SETTINGS'] = {
            'input_file':'',
            'valid_chars':'',
            'auto_bug_report':'Yes',
            'valid_chars_dropdown':'',
            'valid_chars_dropdown_custom':'custom_chars_to_be_replaced_and_saved'}

def read_setting(section, key):
    if config.has_option(section, key):
        return config.get(section, key)
    else:
        return ''

load_settings()

root_window = tk.Tk()
root_window.title("[Spac3] find unexpected characters")
root_window.config(bg="skyblue")
root_window.maxsize(900, 600)

top_frame = tk.Frame(root_window, width=600, bg="gray")
top_frame.grid(row=0, column=0, padx=1, pady=2)

input_label = tk.Label(top_frame, text="Top frame")
input_label.grid(row=0, column=0, padx=1, pady=1)
widgets_added.append(input_label)


mid_frame = tk.Frame(root_window, width=600)
mid_frame.grid(row=1, column=0, padx=1, pady=2)


input_label = tk.Label(mid_frame, text="Input File: ")
input_label.grid(row=0, column=0)
widgets_added.append(input_label)

input_field = tk.Entry(mid_frame, textvariable=read_setting('SETTINGS','input_file'), width=50)
input_field.grid(row=0, column=1)
widgets_added.append(input_field)

browse_button = tk.Button(mid_frame, text="Browse", command=browse_file)
browse_button.grid(row=0, column=2)
widgets_added.append(browse_button)


chars_label = tk.Label(mid_frame, text="Valid Chars: ")
chars_label.grid(row=1, column=0)
widgets_added.append(chars_label)

valid_chars_var = tk.StringVar(mid_frame)
valid_chars_var.trace("w", set_valid_chars)
chars_field = tk.Entry(mid_frame, textvariable=read_setting('SETTINGS','valid_chars'), width=50)
chars_field.grid(row=1, column=1)
widgets_added.append(chars_field)


valid_chars_dropdown_label = tk.Label(mid_frame, text="Valid Chars presets: ")
valid_chars_dropdown_label.grid(row=2, column=0)
widgets_added.append(valid_chars_dropdown_label)

valid_chars_dropdown = ttk.Combobox(mid_frame, textvariable=valid_chars_var)
valid_chars_dropdown.grid(row=2, column=1)
valid_chars_dropdown['values'] = ('Lowercase', 'AlphaNumeric', 'Custom')
valid_chars_dropdown.set(read_setting('SETTINGS','valid_chars_dropdown'))
widgets_added.append(valid_chars_dropdown)

bot_frame = tk.Frame(root_window, width=600)
bot_frame.grid(row=2, column=0, padx=1, pady=2)

run_button = tk.Button(bot_frame, text="Run Program", command=lambda : run_program())
run_button.grid(row=3, column=1)
widgets_added.append(run_button)

save_button = tk.Button(bot_frame, text="Save settings", command=lambda : save_settings())
save_button.grid(row=3, column=0)
widgets_added.append(save_button)

try:
    root_window.mainloop()
except KeyboardInterrupt:
    pass