import socket

def status(proto, port, status):
    print(f'{proto}:{str(port)} -> {status}')

def ScanTCPPort(hostname, proto, from_port, to_port, content="default data".encode()):
    for port in range(from_port, to_port +1): # +1 because 80:80 is one port, but to get one port it wants 80:81
        try:
            this_protocol = proto.upper()
            timeout_sec = 0.5
            if this_protocol == "TCP":
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.settimeout(timeout_sec)
                s.connect((hostname, port))
                status(proto=this_protocol, port=port, status='Open (Connected)')
            elif this_protocol == "UDP":
                s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                s.settimeout(timeout_sec)
                s.sendto(content, (hostname, port))
                print ((s.recvfrom(1024)))
            while 1:
                data = s.recv(1024)
                if len(data) == 0:
                    break
                print("Received:", repr(data))
            s.close()
        except ConnectionRefusedError as err:
            status(proto=this_protocol, port=port, status='Closed (Refused)')
        except TimeoutError as err:
            #status(proto=this_protocol, port=port, status='Closed (Dropped)')
            pass
    # s.sendall(content)
    # s.shutdown(socket.SHUT_WR)
    # print("Connection closed.")
        
ScanTCPPort('127.0.0.1', "udp", 10, 65535)