# Avoid using config.yaml as filename in your add-on for anything other than the add-on configuration.
# The Supervisor does a recursively search for config.yaml in the add-on repository.
# https://developers.home-assistant.io/docs/add-ons/configuration

import sqlite3
from flask import Flask, render_template, send_from_directory

app = Flask(__name__)

def get_db_connection():
    conn = sqlite3.connect('database.db')
    conn.row_factory = sqlite3.Row
    return conn

@app.route('/')
def index():
    #conn = get_db_connection()
    #posts = conn.execute('SELECT * FROM posts').fetchall()
    #conn.close()
    # return render_template('pages-blank.html')
    # return render_template('index.html', posts=posts)
    return "Hello world"

@app.route('/assets/<path:name>')
def assets(name):
    print(f'requested path: {name}')
    return send_from_directory('assets', name, as_attachment=False)




if __name__ == '__main__':
      app.run(host='0.0.0.0', port=8099)