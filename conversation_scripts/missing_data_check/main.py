import http.client
import smtplib
import configparser
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def read_config(config_file):
    config = configparser.ConfigParser()
    config.read(config_file)
    return config

def check_webpage(fqdn, path, string, smtp_details):
    try:
        connection = http.client.HTTPSConnection(fqdn)
        connection.request("GET", path)
        response = connection.getresponse()
        if response.status != 200:
            error_message = f"Error fetching the page: {response.status} {response.reason}"
            print(error_message)
            send_email(smtp_details, fqdn, path, string, error_message)
            return False
        source_code = response.read().decode('utf-8')
    except Exception as e:
        error_message = f"Error downloading the page: {e}"
        print(error_message)
        send_email(smtp_details, fqdn, path, string, error_message)
        return False
    finally:
        connection.close()

    # Check if the string is in the source code
    if string in source_code:
        print(f"String found in the source code of {fqdn}{path}.")
        return True
    else:
        error_message = None
        print(f"String not found in the source code of {fqdn}{path}.")
        send_email(smtp_details, fqdn, path, string, error_message)
        return False

def send_email(smtp_details, fqdn, path, string, error_message):
    smtp_server = smtp_details["smtp_server"]
    smtp_port = smtp_details.getint("smtp_port")
    sender_email = smtp_details["sender_email"]
    sender_password = smtp_details["sender_password"]
    recipient_email = smtp_details["recipient_email"]
    subject = smtp_details["subject"]

    if error_message:
        body = smtp_details["error_body_template"].format(
            fqdn=fqdn,
            path=path,
            expected_string=string,
            error_message=error_message
        )
    else:
        body = smtp_details["body_template"].format(
            expected_string=string,
            fqdn=fqdn,
            path=path
        )

    # Create the email
    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = recipient_email
    message["Subject"] = subject
    message.attach(MIMEText(body, "plain"))

    # Send the email
    try:
        with smtplib.SMTP_SSL(smtp_server, smtp_port) as server:
            server.login(sender_email, sender_password)
            server.sendmail(sender_email, recipient_email, message.as_string())
        print(f"Email sent successfully for {fqdn}{path}!")
    except Exception as e:
        print(f"Error sending email for {fqdn}{path}: {e}")

def check_websites(config):
    smtp_details = config["Email"]

    for section in config.sections():
        if section != "Email":
            fqdn = config[section]["fqdn"]
            path = config[section].get("path", "/")  # default to `/` if not specified
            expected_string = config[section]["expected_string"]

            check_webpage(fqdn, path, expected_string, smtp_details)

if __name__ == "__main__":
    config_file = "config.ini"  # Hardcoded path to the INI file
    config = read_config(config_file)

    check_websites(config)