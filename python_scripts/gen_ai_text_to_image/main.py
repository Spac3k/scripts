from  constants import CONFIG_LLM_FILENAME, CONFIG_HUGGINGFACE_HUB_TOKEN, CONFIG_PROMPT, CONFIG_NEGITIVE_PROMPT, CONFIG_LLM_MODEL,\
    OUTPUT_FILE_NAME

from huggingface_hub import login
from diffusers import DiffusionPipeline, StableDiffusionPipeline
import torch
import random


# login( token = CONFIG_HUGGINGFACE_HUB_TOKEN)

if False: #  offline model
    pipe = StableDiffusionPipeline.from_single_file(CONFIG_LLM_FILENAME) #  .safetensors
else:
    # download model
    pipe = DiffusionPipeline.from_pretrained(
        CONFIG_LLM_MODEL, 
        torch_dtype=torch.float32, safety_checker = None
    )
#pipe.scheduler = DPMSolverMultistepScheduler.from_config(pipe.scheduler.config)
# pipe.vae = AutoencoderKL.from_pretrained(llCONFIG_LLM_MODELm, torch_dtype=torch.float32, safety_checker = None)


# Setting for image generation
num_steps = 40
num_variations = 1
prompt_guidance = 9
dimensions = (400, 600) # (width, height) tuple
random_seeds = [random.randint(0, 65000) for _ in range(num_variations)]
print(f'seeds: {str(random_seeds)}')
images = pipe(
        prompt= num_variations * [CONFIG_PROMPT],
        negative_prompt= num_variations * [CONFIG_NEGITIVE_PROMPT],
        num_inference_steps=num_steps,
        guidance_scale=prompt_guidance,
        height = dimensions[0],
        width = dimensions[1],
        generator = [torch.Generator().manual_seed(i) for i in random_seeds]
    ).images

for pic in images:
    pic.show()
    pic.save(f"{OUTPUT_FILE_NAME}.png", quality=100, save_all=True)
