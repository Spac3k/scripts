## Description
This script takes in a text file and iterate over each character, these characters are then compared to a "valid character" expression. If an invalid character is found, then it will log this in results.txt.

## Status 
 - [x] Concept
 - [_] Experimental
 - [_] Integrated/Using

## Disclaimer
This code it open source for you to read and learn from. You run it at your own risk.

## How to use
- sudo apt-get install python3-pip
- python3 -m pip install --upgrade pip
- python3 -m pip install -r requirements.txt
- python3 char_filter.py

## Package for windows
- python3 -m pip install pyinstaller
- python3 -m pyinstaller --onefile main.py
