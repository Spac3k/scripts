import os
from difflib import SequenceMatcher
import json
import logging 
import re
from datetime import datetime
import math

import requests
from contstants import INPUT_DOWNLOAD_FOLDER, CONFIG_MEDIA_TYPE, CONFIG_THEMOVIEDB_API_KEY, OUTPUT_FOLDER


# folder_path = r'C:\tmp\movies\New folder\from'
folder_path = INPUT_DOWNLOAD_FOLDER
new_folder = OUTPUT_FOLDER
media_type = CONFIG_MEDIA_TYPE # movie / tv
api_read_token = CONFIG_THEMOVIEDB_API_KEY
lookup_good_count = 0
lookup_bad_count = 0
lookup_cache = {}

log = logging.getLogger(__name__) 
log.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(levelname)s %(asctime)s - %(message)s") 

file_handler = logging.FileHandler("logging.txt")
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter) 
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
console_handler.setFormatter(formatter)

log.addHandler(file_handler) 
log.addHandler(console_handler) 

# logging messages 
log.info("Program started")

def compare(string_1, string_2):

    return SequenceMatcher(None, string_1.lower(), string_2.lower()).ratio()

def test_for_int(input_string):
    try:
        new_int = int(input_string)
        return new_int
    except ValueError:
        return False

def clean_file_name(file_name):
    new_name = file_name.lower()
    found_year = 0
    found_season = 0
    found_epp = 0
    bad_input = [
                "WEBRip", "BluRay", "BRRip", "DUBBED", "AMZN", "WEB-DL", "WEBDL", "web", "HDRip", "BD25", "DSNP", "IMAX", "dvdrip", "ROKU", ".nf.",
               "480p", "1080p", "2160p", "UHD", "720p", "4k",
               "10bit", "H264", "H.264", "x264", "x265", "AAC5.1", "AAC2.0", "AAC", "DDP5.1.Atmos", "DD5.1", "DDP5.1","XviD", "AC3",
               "RARBG", "ION10", "YIFY", "rovers", "w4f", "yts.mx",
               "PROPER", "korean", "3D", "korsub",
               ".m2ts", ".mkv", ".avi", ".mp4",
               "-FGT", "-CMRG", "-CM", "-EVO", "-shitbox", "-NOGRP", "-SMURF", "-CMRG", "-FGT[EtHD]", "-pawel2006", "-trump", "-TERMiNAL", "-TIMECUT", "-MRCS", "-naisu", "LEGi0N", "-TEPES", "-KOGi", "-FLUX", "-EddieSmurfy",
               "-", "_", "(", ")", ".", "[", "]", ":", "!"]
    regex_pattern = r'S\d\dE\d\d'
    regex = re.compile(regex_pattern, re.IGNORECASE)
    match = re.search(regex, new_name)
    if match:
        match_start, match_end = match.span()
        log.debug(f"regex match span: {match_start} -> {match_end} on {new_name} looks like {new_name[match_start:match_end]}")
        regex_string_match = new_name[match_start:match_end]
        found_season = regex_string_match[1:3]
        found_epp = regex_string_match[4:6]
        #log.debug(f"S{found_season}E{found_epp}")
        new_name = new_name[0:match_start]

    regex_pattern = r'\s\d\.\d{1,3}'
    regex = re.compile(regex_pattern, re.IGNORECASE)
    match = re.search(regex, new_name)
    if match:
        match_start, match_end = match.span()
        regex_string_match = new_name[match_start:match_end]
        new_name = new_name[0:match_start]


    for bad_input_item in bad_input:
        new_name = new_name.replace(bad_input_item.lower(), " ")
    new_name_parts = new_name.split(" ")
    for part in new_name_parts:
        try:
            test_part = test_for_int(part)
            if test_part != False and int(test_part) > 1900 and int(test_part) < 2040:
                found_year = str(test_part)
                new_name = new_name.replace(found_year, "")
                break
        except ValueError:
            pass
    new_name = " ".join(new_name.split())
    return new_name, found_year, found_season, found_epp

def tvdb_query(input_name, media_type):
    global lookup_good_count, lookup_bad_count, lookup_cache
    log.debug(f"input_name: {input_name}")
    result_name = ""
    release_date = 0
    rating = 0
    cleaned_name, release_year, found_season, found_epp = clean_file_name(input_name)

    if cleaned_name not in lookup_cache.keys():
        url = "https://api.themoviedb.org/3/search/"+media_type+"?query="+cleaned_name+"&include_adult=true&language=en-US&page=1"
        if int(release_year) > 0:
            log.debug(f"Year ({release_year}) added to search: {cleaned_name}")
            url = url + "&year=" + release_year

        headers = {
            "accept": "application/json",
            "Authorization": "Bearer " + api_read_token
        }

        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            payload = json.loads(response.text)
            try:
                first_result = payload["results"][0]
                if media_type == "movie":
                    result_name = first_result["title"]
                    release_date = first_result["release_date"]
                    rating = first_result["vote_average"]
                elif media_type == "tv":
                    result_name = first_result["name"]
                    release_date = first_result["first_air_date"]
                    rating = first_result["vote_average"]

                name_test_ratio = compare(cleaned_name.lower().replace("the ", ""), result_name.lower().replace("the ", ""))
                if name_test_ratio < 0.9:
                    lookup_bad_count += 1
                    log.warning(f"Questionable match: {name_test_ratio} | Input name: {cleaned_name} ({release_year}) / {input_name} | Result name: {result_name} ({release_date})")
                    if True:
                        answer = input("Accept match?").lower()
                        if "y" in answer:
                            lookup_cache[cleaned_name] = {}
                            lookup_cache[cleaned_name]["title"] = result_name
                            lookup_cache[cleaned_name]["release_date"] = release_date
                            lookup_cache[cleaned_name]["vote_average"] = rating
                            lookup_cache[cleaned_name]["found"] = True
                        else:
                            lookup_cache[cleaned_name] = {}
                            lookup_cache[cleaned_name]["title"] = result_name
                            lookup_cache[cleaned_name]["found"] = False
                    else:
                        lookup_cache[cleaned_name] = {}
                        lookup_cache[cleaned_name]["title"] = result_name
                        lookup_cache[cleaned_name]["found"] = False
                else:
                    lookup_good_count += 1
                    log.debug(f"Good match: {name_test_ratio} | Input name: {cleaned_name} / {input_name} | Result name: {result_name}")
                    lookup_cache[cleaned_name] = {}
                    lookup_cache[cleaned_name]["title"] = result_name
                    lookup_cache[cleaned_name]["release_date"] = release_date
                    lookup_cache[cleaned_name]["vote_average"] = rating
                    lookup_cache[cleaned_name]["found"] = True
            
            except IndexError:
                if test_for_int(cleaned_name) > 0:
                    lookup_cache[cleaned_name] = {}
                    lookup_cache[cleaned_name]["found"] = False
                else:
                    log.warning(f"Can't find: {input_name} / {cleaned_name}")
                    lookup_bad_count += 1
                    lookup_cache[cleaned_name] = {}
                    lookup_cache[cleaned_name]["found"] = False
    else:
        log.debug(f"cache hit for: {cleaned_name}")
        if lookup_cache[cleaned_name]["found"]:
            result_name = lookup_cache[cleaned_name]["title"]
            release_date = lookup_cache[cleaned_name]["release_date"]
            rating = lookup_cache[cleaned_name]["vote_average"]
    return lookup_cache[cleaned_name]["found"], result_name, release_date, rating, found_season, found_epp

def move_file(file_path, new_directory, sub_folder, new_file_name):
    save_path = os.path.join(new_directory, sub_folder)
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    old_absolute_path = os.path.abspath(file_path)
    file_extention_index = file_path.rindex(".")
    file_extention = file_path[file_extention_index : len(file_path)]
    new_absolute_path = os.path.join(save_path, new_file_name + file_extention)
    log.info(f'Moving {old_absolute_path} to {new_absolute_path}')
    try:
        os.rename(old_absolute_path, new_absolute_path)
    except FileExistsError:
        log.info(f'File already exists, skipping: {new_absolute_path}')
    #shutil.copyfile(old_absolute_path, new_absolute_path)

    #log.info("pause")
    #time.sleep(10)

def check_folder(folder_path):
    
    # Get files in the folder
    for file_name in os.listdir(folder_path):
        # Recursively check sub-folders
        full_path = os.path.join(folder_path, file_name)
        if os.path.isdir(full_path):
            check_folder(full_path)
        else:
            # Check the size of each file
            if os.stat(os.path.join(folder_path, file_name)).st_size > 100000000:
                # Split file name from the extension
                try:
                    log.debug(f"Sending file for search: {full_path}")
                    result, name, date, rating, season, epp = tvdb_query(file_name, media_type)
                    rating_floor = math.floor(float(rating))
                    if result:
                        release_date = datetime.strptime(date, '%Y-%m-%d')
                        release_year_string = release_date.strftime("%Y")
                        cleaned_file_name = re.sub(r'[^A-Za-z0-9 ]+', '', name)
                        if media_type == 'movie':
                            sub_folder = f'{rating_floor}_{release_year_string}_{cleaned_file_name}'
                            new_file_name = f'{cleaned_file_name} ({release_year_string})'
                        elif media_type == 'tv':
                            sub_folder = f'{name}'
                            new_file_name = f'{cleaned_file_name} - S{season}E{epp}'
                        sub_folder = sub_folder.replace(":"," ")
                        sub_folder = sub_folder.replace("!"," ")
                        sub_folder = sub_folder.strip()
                        move_file(full_path, new_folder, sub_folder, new_file_name)                        
                except ValueError as e:
                    log.warning(f"Problem with file: {full_path} ... {e}")

def remove_empty_folders(path):
    folders = list(os.walk(path))[1:]

    for folder in folders:
        # folder example: ('FOLDER/3', [], ['file'])
        if not folder[2]:
            delete_empty_dir(folder[0])

def delete_empty_dir(path):
    if os.path.isdir(path):
        if not os.listdir(path):
            os.rmdir(path)
            log.info(f"Directory deleted: {path}")
        else:    
            log.info(f"Directory is not empty: {path}")
    else:
        log.error(f"Given directory doesn't exist: {path}")

# Initiate recursive check

check_folder(folder_path)
remove_empty_folders(folder_path)
# # move_file( r"C:\tmp\movies\New folder\from\Evil.Dead.Rise.2023.1080p.WEBRip.x264-RARBG\Evil.Dead.Rise.2023.1080p.WEBRip.x264-RARBG.mp4" ,  r"C:\tmp\movies\New folder\to", "new file")
log.info(f"Total good lookups: {lookup_good_count} vs bad lookups: {lookup_bad_count}")