# How to get the video came from: https://stackoverflow.com/a/53105237/5573471
import os
import re

def get_offset(file):
    with open(file, 'rb') as f:
        content = f.read()
    match = re.search(b'ftypmp4', content)
    return match.start() - 4 if match else None

for filename in os.listdir('.'):
    if filename.startswith("test") and filename.endswith(".heic"):
        ofs = get_offset(filename)
        if ofs:
            with open(filename, 'rb') as old_file:
                with open(f'{filename[:-4]}.mp4', 'wb') as new_file:
                    old_file.seek(ofs)
                    new_file.write(old_file.read())
