from pathlib import Path
from PIL import Image
import pillow_heif

# This demo converts gif to heic.
if __name__ == "__main__":
    pillow_heif.register_heif_opener()
    # os.makedirs(TARGET_FOLDER, exist_ok=True)
    image_path = Path("test.heic")
    img = Image.open(image_path)
    img.save(f"{image_path.stem}.png", quality=100, save_all=True)
    img.close()