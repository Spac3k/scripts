import cv2
from constants import INPUT_VIDEO_FILENAME, \
    CONFIG_FRAME_NUMBER_START, CONFIG_FRAME_SKIP_COUNT, CONFIG_FRAME_NUMBER_END, \
    OUTPUT_FOLDER_NAME, OUTPUT_GIF_FILE_NAME


import glob
from PIL import Image

class VidToImg():
    def __init__(self):
        self.start_at_frame = 0
        self.frame_return_count = 1
        self.frame_skip = 0
        self.current_frame = 0
        self.frame_total_count = 0
        self.video_fps = 0

    def get_frame_number_by_time_offset(self, hours=0, minutes=0, seconds=0):
        return(int(self.video_fps*(hours*60*60 + minutes*60 + seconds)))

    
    def open_video(self, file_path):
        self.video_capture = cv2.VideoCapture(file_path)
        self.frame_total_count = self.video_capture.get(cv2.CAP_PROP_FRAME_COUNT)
        self.video_fps = round(self.video_capture.get(cv2.CAP_PROP_FPS))
        print(f'This mp4 has {self.frame_total_count} frames at {self.video_fps} frames per second')
        self.video_capture.release


    def export_frames_test(self, file_name, frame_no):
        cap = cv2.VideoCapture(file_name) #video_name is the video being called
        cap.set(cv2.CAP_PROP_POS_FRAMES,frame_no); # Where frame_no is the frame you want
        ret, frame = cap.read() # Read the frame
        cv2.imwrite(f"{OUTPUT_FOLDER_NAME}/frame_{frame_no:03d}.jpg", frame)
        #cv2.imshow('window_name', frame) # show frame on window
        #If you want to hold the window, until you press exit:

        # while True:
        #     ch = 0xFF & cv2.waitKey(1) # Wait for a second
        #     if ch == 27:
        #         break
        
    def export_frames(self):
        print('export_frames')
        self.current_frame = self.start_at_frame
        self.video_capture.set(1, self.current_frame -1 )
        still_reading, image = self.video_capture.read()
        # print(f'export_frames: image: ms in vid: {self.video_capture.get(cv2.CV_CAP_PROP_POS_MSEC)}')
        cv2.imwrite(f"{OUTPUT_FOLDER_NAME}/frame_{self.current_frame:03d}.jpg", image)
        print(f'export_frames: before loop, current_frame: {self.current_frame} :: {self.start_at_frame+self.frame_return_count}')
        if self.frame_skip != 0:
            print(f'mod({str(self.current_frame)},{str(self.frame_skip)}): {str(self.current_frame%self.frame_skip)}')
        while still_reading and self.current_frame <= self.start_at_frame:
            self.current_frame += 1
            self.video_capture.set(cv2.CAP_PROP_POS_FRAMES, self.current_frame)
            still_reading, image = self.video_capture.read()
            print('.', end='', flush=True)

        while still_reading and self.current_frame <= self.start_at_frame+self.frame_return_count:
            print(f'\export_frames: image: output/frame_{self.current_frame:03d}.jpg')
            cv2.imwrite(f"{OUTPUT_FOLDER_NAME}/frame_{self.current_frame:07d}.jpg", image)
            if self.frame_skip > 0:
                self.current_frame = self.current_frame + self.frame_skip
            else:
                self.current_frame += 1
            self.video_capture.set(cv2.CAP_PROP_POS_FRAMES, self.current_frame)
            still_reading, image = self.video_capture.read()
        print('\nconvert_mp4_to_jpgs: done')

def make_gif(frame_folder, file_name):
    images = glob.glob(f"{frame_folder}/*.jpg")
    images.sort()
    frames = [Image.open(image) for image in images]
    frame_one = frames[0]
    frame_one.save(f"{file_name}.gif", format="GIF", append_images=frames,
                save_all=True, duration=200, loop=1)
        
if __name__ == "__main__":
    # 10 snips
    file = INPUT_VIDEO_FILENAME
    round_1 = VidToImg()
    round_1.open_video(file)
    # print(f'At {round_1.video_fps} fps with {round_1.frame_total_count} frames, the video should be {round((round_1.frame_total_count/round_1.video_fps) / 60) } mins long')
    
    #round_1.start_at_frame = round_1.get_frame_number_by_time_offset(hours=0, minutes=12, seconds=41)
    #print(round_1.start_at_frame)
    round_1.start_at_frame = CONFIG_FRAME_NUMBER_START
    print(round_1.start_at_frame)
    #end_frame = round_1.get_frame_number_by_time_offset(hours=0, minutes=13, seconds=0)
    end_frame = CONFIG_FRAME_NUMBER_END
    print(end_frame)
    round_1.frame_return_count = end_frame - round_1.start_at_frame
    #round_1.frame_return_count = 28045 - round_1.start_at_frame
    round_1.frame_skip = CONFIG_FRAME_SKIP_COUNT
    round_1.export_frames()

    make_gif(OUTPUT_FOLDER_NAME, f'{ OUTPUT_GIF_FILE_NAME }.gif')
