import time, hashlib
import os
import datetime
from exif import Image
import plum.exceptions as plum_exceptions
from PIL import Image as PIL_Image
from PIL.ExifTags import TAGS
from PIL.PngImagePlugin import PngImageFile, PngInfo
from rawphoto.cr2 import Cr2

from pathlib import Path
import pandas as pd
import logging
import re

import pillow_heif
import cv2

log = logging.getLogger()

def build_cache():
    pass

def get_files_in_path(path,get_ext="jpg,dng,png,gif,test,heic,mp4,m4v,cr2"): # 
    files = []
    #get_ext = get_ext.split(",")
    if os.name == 'nt': # for windows
        # change backslash to forward slash
        path = path.replace('\\','/')
    items = os.listdir(path)
    for full_file_name in items:
        full_path_file = os.path.join(path, full_file_name)
        if os.path.isfile(full_path_file):
            file_name, file_ext = get_file_name_ext(full_path_file)
            if file_ext.lower() in get_ext:
                files.append(full_path_file)
        elif os.path.isdir(full_path_file):  
            results = get_files_in_path(full_path_file, get_ext)
            files.extend(results)
        else:
            pass # don't care for special files like sockets or links

    return files

def get_file_name_ext(full_file_name):
    ext_pos = full_file_name.rfind(".")
    file_ext = full_file_name[ext_pos +1 :]
    file_name_pos_1 = full_file_name.rfind("/")
    file_name_pos_2 = full_file_name.rfind("\\")
    if file_name_pos_1 > file_name_pos_2:
        file_name_pos = file_name_pos_1
    else:
        file_name_pos = file_name_pos_2
    file_name = full_file_name[file_name_pos + 1: ext_pos]
    return (file_name, file_ext)

def duplicate_file_best_name(file_list):
    # checkign if any file as markers that could indecate it is a duplicate
    duplicate_indecator = ["copy", "duplicate", "(", ")"]
    keep_file_list = None
    file_tracking = {}
    for this_file in file_list:
        file_tracking[this_file] = 0
        for text in duplicate_indecator:
            if text in this_file.lower():
                #log.info("point to {} for containing {}".format(this_file, text))
                file_tracking[this_file] += 1
    
    # checking if one file name is in another
    for this_file in file_list:
        temp = []
        temp.extend(file_list)
        temp.remove(this_file)
        file_name, file_ext = get_file_name_ext(this_file)
        for comparing_file in temp:
            if file_name in comparing_file:
                #log.info("point removed from {} for containing {}".format(this_file, comparing_file))
                file_tracking[this_file] -= 1
                
    # Converting to dataframe  
    df = pd.DataFrame(list(file_tracking.items()), columns = ['Filename', 'Points']) 
  
    # return filename with lowest amount of points
    lowest = df['Points'].min()
    keep_file_list = df[df['Points'] == lowest]['Filename'].values.tolist()
    #log.info(keep_file_list)
    return keep_file_list

def hash(input_payload, BUF_SIZE=pow(2, 18), hash="sha256", input_payload_is_file=True):

    # A arbitrary (but fixed) buffer
    # size (change accordingly)
    # 65536 = 65536 bytes = 64 kilobytes
    # BUF_SIZE = 65536

    if "sha256" in hash:
        hash_lib = hashlib.sha256()
    elif "md5" in hash:
        hash_lib = hashlib.md5()

    timestamp = time.time()
    # sha256 = hashlib.sha256()
    if input_payload_is_file:
        with open(input_payload, 'rb') as f:
            while True:
                data = f.read(BUF_SIZE)
                # True if eof = 1
                if not data:
                    break
                hash_lib.update(data)
    else:
        # log.info("string hash")
        str_input = str(input_payload).encode()
        hash_lib.update(str_input)

    str_hash = hash_lib.hexdigest()
    # log.info("Buffer: {} | Time delay: {} | Hash: {}".format(str(BUF_SIZE), time.time() - timestamp, str_hash))
    return str_hash


def file_move(str_src_file_fullpath, str_dst_dir):

    if os.path.isfile(str_src_file_fullpath):
        os.makedirs(str_dst_dir, exist_ok=True)
        if os.path.isdir(str_dst_dir):
            # move the image from its original location to this folder
            file_name, file_ext = get_file_name_ext(str_src_file_fullpath)
            new_location = os.path.join(str_dst_dir, file_name+"."+file_ext)
            try:
                os.rename(str_src_file_fullpath, new_location)
                log.info(f'Moved file file to {new_location}')
            except FileExistsError:
                log.warning(f'File already exists at {new_location}')
                source_file = hash(str_src_file_fullpath)
                destination_file = hash(new_location)
                if source_file == destination_file:
                    log.info(f"Duplicate file ({source_file}), renaming")
                    os.rename(str_src_file_fullpath, str_src_file_fullpath + ".duplicate")
                else:
                    new_location = os.path.join(str_dst_dir, file_name+"-name_conflict."+file_ext)
                    os.rename(str_src_file_fullpath, new_location + ".duplicate")
                    log.info(f"It has the same name, but not the same content. Rename and move: {new_location}")
        else:
            raise "folder missing"
    else:
        raise "invalid input"

def print_entries(raw, ifd, level=1):
    for name in ifd.entries:
        e = ifd.entries[name]
        if name in ifd.subifds or isinstance(name, tuple):
            if isinstance(name, tuple):
                for n in name:
                    print(level * "\t" + n + ":")
                    print_entries(raw, ifd.subifds[n], level + 1)
            else:
                print(level * "\t" + name + ":")
                print_entries(raw, ifd.subifds[name], level + 1)
        else:
            if isinstance(name, str):
                if e.tag_type_key == 0x07:
                    print(level * "\t" + "{}: {}".format(
                        name,
                        "[Binary blob]"
                    ))
                else:
                    print(level * "\t" + "{}: {}".format(
                        name,
                        ifd.get_value(e)
                    ))

def read_file_date(file_path):
    file_name_only, file_ext = get_file_name_ext(file_path)

    metadata_date_found=False
    if not metadata_date_found and 'gif' not in file_ext:
        log.debug(f'Checking file for metadata date: {file_path}')
        try:
            if '.cr2' in file_path.lower():
                metadata = {}
                (filepath_no_ext, ext) = os.path.splitext(file_path)
                # filename_no_ext = os.path.basename(filepath_no_ext)
                ext = ext.lower()
                if ext == '.cr2':
                    raw = Cr2(filename=file_path)
                    metadata_date_str = raw.ifds[0].get_value(raw.ifds[0].entries['datetime'])
                    metadata_date = datetime.datetime.strptime(metadata_date_str, "%Y:%m:%d %H:%M:%S")
                    metadata_date_found = True
                    # print(raw.ifds[0].entries['datetime'].raw_value)
                    
                    # for i in range(len(raw.ifds)):
                    #     ifd = raw.ifds[i]
                    #     print("IFD #{}".format(i))
                    #     print_entries(raw, ifd)
                    #     # Hax.
                    #     for subifd in ifd.subifds:
                    #         if isinstance(subifd, int):
                    #             print("Subifd ", subifd)
                    #             print_entries(raw, ifd.subifds[subifd], 1)
                    raw.close()
                # elif ext == '.NEF':
                #     raw = Nef(filename=filepath)
                else:
                    raise TypeError("Format not supported")
                    for i in range(len(raw.ifds)):
                        ifd = raw.ifds[i]
                        print("IFD #{}".format(i))
                        raw_metadata(raw, ifd)
                        for subifd in ifd.subifds:
                            if isinstance(subifd, int):
                                print("Subifd ", subifd)
                                raw_metadata(raw, ifd.subifds[subifd], 1)
                    raw.close()
            elif '.mp4' in file_path.lower():
                log.info("Skipping mp4 metadata read")
                pass
                # app_path = os.path.join(ffmpeg.__file__)
                # os.environ["PATH"] += os.pathsep + app_path
                
                # file_path_fix = file_path.replace("\\", "/")
                # print(file_path_fix)
                # print(ffmpeg.probe(file_path_fix)["streams"])
                # raise
            elif '.jpg' in file_path.lower():
                with open(file_path, 'rb') as image_file:
                    my_image = Image(image_file)
                    
                    if hasattr(my_image, "datetime_original"):
                        log.info("Using datetime_original")
                        metadata_date_str = str(my_image.datetime_original)
                        metadata_date_found = True
                    elif hasattr(my_image, "datetime"):
                        log.info("Using datetime")
                        metadata_date_str = str(my_image.datetime)
                        metadata_date_found = True

                    if metadata_date_found:
                        metadata_date = datetime.datetime.strptime(metadata_date_str, "%Y:%m:%d %H:%M:%S")
            elif '.png' in file_path.lower():
                # from PIL import Image
                pass 
                # im = Image(file_path)
                # im.load()  # Needed only for .png EXIF data (see citation above)
                # log.error(im.info.keys())

                # with open(file_path, 'rb') as image_file:
                #     my_image = Image(image_file)
                #     image = PngImageFile(my_image) 
                #     metadata = PngInfo()
                #     log.debug(str(metadata))
        except plum_exceptions.UnpackError as e:
            # plum.exceptions.UnpackError:
            # ValueError occurred during unpack operation:
            # 23575 is not a valid TiffByteOrder
            log.warning(f'{str(e)} | {str(file_path)}', exc_info=True)
            pass
        except KeyError as e:
            log.warning(f'KeyError: {str(e)} | {str(file_path)}', exc_info=True)
            pass
        except AttributeError as e:
            log.warning(f'AttributeError: {str(e)} | {str(file_path)}', exc_info=True)
            pass

    date_syntaxs = [
        #[int_char_start, int_char_end, regex],
        [0,15,"%Y%m%d_%H%M%S"],
        [0,15,"%Y%m%d %H%M%S"], # 20150614 002349
        [0,19,"%Y-%m-%d %H.%M.%S"],
        [0,14,"%Y%m%d%H%M%S"],
        [4,99,"%Y%m%d_%H%M%S"],  # IMG_
        [4,12,"%Y%m%d"], #IMG_
        [0,10,"%Y-%m-%d"], # 2012-07-27-063
        [0,8,"%d%m%Y"], # 07012012140.test" # Saturday, 14 January 2012, 01:02:55
        [8,28,"%Y-%m-%d-%Hh%Mm%Ss"], # vlcsnap-2024-06-06-23h25m31s649.png
       #[11,26,"%Y%m%d_%H%M%S"] # Screenshot_20240331_181857
        [11,26,"%Y%m%d-%H%M%S"], # Screenshot-20240331-181857
        [4,12,"%Y%m%d" ], # VID-20231212-WA0002
        [17,32,"%Y%m%d_%H%M%S"] # Screen_Recording_20240703_011857_Camera
    ]
    
    filename_date_found=False
    for date_syntax in date_syntaxs:
        if not filename_date_found:
            int_char_start = date_syntax[0]
            int_char_end = date_syntax[1]
            str_regex = date_syntax[2]

            try:
                file_name_temp = file_name_only[int_char_start:int_char_end]
                log.debug(f'Checking file name for date: {file_name_temp}')
                filename_date = datetime.datetime.strptime(file_name_temp, str_regex)
                now = datetime.datetime.now()
                if int(filename_date.strftime("%Y")) < 2008 or int(filename_date.strftime("%Y")) > int(now.strftime("%Y")):
                    log.debug(f'invalid date filename_date: {filename_date} from filename:  {file_name_temp}')
                    continue
                filename_date_found = True
                log.debug(f'Date found: {filename_date.strftime("%Y%m%d_%H%M%S")} for {file_name_temp}')
                file_date_source = "filename ({}) using {} => {} ".format(file_name_temp, str_regex, filename_date.strftime("%Y%m%d_%H%M%S"))
            except ValueError as err:
                log.debug(str(err))
                pass
    
            
    if metadata_date_found and filename_date_found:
        log.debug('Both file and metadata has dates')
        # print(f'metadata_date: {metadata_date} | filename_date: {filename_date} | file_date_source: {file_date_source}')
        
        if metadata_date == filename_date:
            log.debug('Both file name and metadata has the same date')
            real_date = metadata_date
            date_source = "Metadata"
        elif metadata_date < filename_date:
            log.debug('Using metadata as date')
            real_date = metadata_date
            date_source = "Metadata"
        elif metadata_date > filename_date:
            log.debug('Using filename as date')
            real_date = filename_date
            date_source = "File name"
    elif metadata_date_found:
        log.debug('Using metadata as date')
        real_date = metadata_date
        date_source = "Metadata"
    elif filename_date_found:
        log.debug('Using filename as date')
        real_date = filename_date
        date_source = "File name"

    if metadata_date_found or filename_date_found:
        log.debug("Date found in : {}".format(date_source))
        return real_date
    else:
        log.error(f"No date found for {file_name_only}")
        return None



def rename_file():
    pass
    # https://www.journaldev.com/23365/python-string-to-datetime-strptime
    file_list = list(Path(".").glob("test_file*"))

    def update_file(file,date):
        log.info("{}/{} - Updating file ({}) date to: {}".format(list_progress, list_size, file, real_date))
        log.info(".", end = '')
        modTime = time.mktime(real_date.timetuple())
        #log.info(modTime)
        try:
            os.utime(file, (modTime, modTime))
        except:
            exit()

    list_size=len(file_list)
    list_progress=0
    for file in file_list:
        list_progress+=1
        real_date=False
        log.info("Updating File: {}".format(file))
        real_date=read_file_date(file)
        if real_date == False:
            log.info("Can't fine date for {} - skip".format(file))
            continue
        update_file(file,real_date)

def load_files_to_db(db, search_dir):
    log.info("Checking for new files")
    for file_path in get_files_in_path(search_dir):
        file_hash = hash(file_path)
        if not db.does_exists(file_hash, file_path):
            db.add_record(file_hash, file_path)
            log.info("New file: " + file_path)

def check_db_for_duplicates_and_move(db, duplicate_dir):
    duplicate_files = db.find_duplicates(keep_entry=False)
    if len(duplicate_files) > 0:
        log.info('DB contain duplicate files')
        # log.info(duplicate_files.sort_values(by=['hash']))

        for file_hash in duplicate_files.hash.unique():
            #log.info(file_hash)
            duplicate_files_for_hash = duplicate_files.loc[duplicate_files['hash'] == file_hash, "path"].to_list()
            #log.info(duplicate_files_for_hash)

            keep_file_names = duplicate_file_best_name(duplicate_files_for_hash)
            phase2_filtering = False
            if keep_file_names is not None:
                if len(keep_file_names) > 1:
                    phase2_filtering = True
                    #log.info("next phase of filtering: {}".format(str(keep_file_names)))
                elif len(keep_file_names) == 1:
                    log.info("Keep: {}".format(str(keep_file_names[0])))
                    duplicate_files_for_hash.remove(keep_file_names[0])
                    for dup_file in duplicate_files_for_hash:
                        file_move(dup_file, duplicate_dir)

            if phase2_filtering:
                earliest_timestamp = None
                file_to_keep = ""
                for dup_file_path in keep_file_names:
                    file_timestamp = read_file_date(dup_file_path)
                    if file_timestamp is not None:
                        if earliest_timestamp is None:
                            earliest_timestamp = file_timestamp
                            file_to_keep = dup_file_path
                        elif file_timestamp < earliest_timestamp:
                            earliest_timestamp = file_timestamp
                            file_to_keep = dup_file_path
                    else:
                        log.info("No file date")
                log.info("Keep {}".format(file_to_keep))
                keep_file_names.remove(file_to_keep)
                for duplicate_file in keep_file_names:
                    file_move(duplicate_file, duplicate_dir)

def sort_files(search_dir, sorted_folder):
    file_list = get_files_in_path(search_dir)
    file_pos = 0
    for file_path in file_list:
        file_pos += 1
        log.info(f'Sorting file ({str(file_pos)}/{str(len(file_list))}): {file_path}')
        date = read_file_date(file_path)
        try:
            year = date.year
            month = date.month
            new_location = f'{sorted_folder}\\{year}\\{month:02d}'
            log.info(f'Relocating file to: {new_location}')
            file_move(file_path, new_location)
        except AttributeError as e:
            log.debug(str(e), exc_info=True)


def ExtractVideoFromImage(filename):
    log.info(f'Extracting video from: {filename}')
    with open(filename, 'rb') as f:
        content = f.read()
    match = re.search(b'ftypmp4', content)

    ofs = match.start() - 4 if match else None
    if ofs:
        with open(filename, 'rb') as old_file:
            movie_file = f'{filename[:-4]}_from_heic.mp4'.replace('..', '.')
            with open(movie_file, 'wb') as new_file:
                old_file.seek(ofs)
                new_file.write(old_file.read())
                log.info(f'Extracted video from: {filename} | {movie_file}')
        return movie_file
                

def ConvertVideoToMp4(input):
    log.warning('Skip converting of Video')
    return
    fourcc: str = "mp4v"
    output = input
    log.info(f'Converting {input} to {output}')
    vidcap = cv2.VideoCapture(input)
    if not fps:
        fps = round(vidcap.get(cv2.CAP_PROP_FPS))
    success, arr = vidcap.read()
    if not frame_size:
        height, width, _ = arr.shape
        frame_size = width, height
    writer = cv2.VideoWriter(
        output,
        apiPreference=0,
        fourcc=cv2.VideoWriter_fourcc(*fourcc),
        fps=fps,
        frameSize=frame_size,
    )
    while True:
        if not success:
            break
        writer.write(arr)
        success, arr = vidcap.read()
    writer.release()
    vidcap.release()

def extract_heic(search_dir):
    
    def extract_image(file_path):
        pillow_heif.register_heif_opener()
        # log.info(f'Trying to open : {file_path}')
        image_path = Path(file_path)
        img = PIL_Image.open(file_path)
        image_file = f"{search_dir}\\{image_path.stem}_from_heic.png"
        img.save(image_file, quality=100, save_all=True)
        img.close()
        log.info(f'Extracted image from: {file_path} | {image_file}')


    file_list = get_files_in_path(search_dir, get_ext="heic")
    file_pos = 0
    for file_path in file_list:
        file_pos += 1
        log.info(f'Found HEIC file ({str(file_pos)}/{str(len(file_list))}): {file_path}')
        extract_image(file_path)
        movie_file = ExtractVideoFromImage(file_path)
        ConvertVideoToMp4(movie_file)

        os.rename(file_path, file_path + ".extracted")

def move_duplicates(search_dir, duplicate_dir):
    file_list = get_files_in_path(search_dir, get_ext="duplicate,extracted")
    file_pos = 0
    for file_path in file_list:
        file_pos += 1
        log.info(f'Moving file ({str(file_pos)}/{str(len(file_list))})')
        file_move(file_path, duplicate_dir)

class Database:
    def __init__(self, csv_file):
        self.db_file = csv_file
        pd.set_option('display.float_format', lambda x: f'{x:.3f}')
        pd.options.display.max_colwidth = 100
        self.load_db()
        self.remove_outdated_hash()
        self.remove_missing_files()

    def load_db(self):
        log.info("Loading DB")
        try:
            self.df = pd.read_csv(self.db_file)
        except FileNotFoundError:
            self.df = pd.DataFrame(columns=['date_created', 'hash', 'path'])

    def remove_outdated_hash(self):
        log.info("Checking for old Hash values")
        old_hash = self.find_duplicates('path')
        if len(old_hash) > 0:
            log.info('DB containts {} old hash for some file(s)'.format(str(len(old_hash))))
            # log.info(self.df.sort_values(by=['hash']))
            self.df.drop(old_hash.index ,inplace=True)
        self.save_db()

    def remove_missing_files(self):
        log.info("checking for missing files")
        all_files =  self.df['path'].tolist()
        for this_file in all_files:
            if not os.path.isfile(this_file):
                log.info("File missing, removing from DB: {}".format(this_file))
                self.df.drop(self.df[(self.df['path'] == this_file)].index ,inplace=True)
        self.save_db()

    def save_db(self):
        log.info("Saving DB")
        self.df.to_csv(self.db_file,index=False,columns=['date_created', 'hash', 'path'])
        self.load_db()

    def add_record(self, hash_val, file_path):
        new_record = pd.DataFrame({'date_created': [str(datetime.datetime.now().timestamp())], 'hash': [hash_val], 'path': [file_path] })
        self.df = pd.concat([self.df, new_record])

    def does_exists(self, hash_val, file_path):
        return self.df[(self.df['hash'] == hash_val) & (self.df['path'] == file_path)]['path'].tolist()

    def find_duplicates(self, column="hash", keep_entry='last'):
        return self.df[self.df.duplicated(column, keep=keep_entry) == True].sort_values(by=['date_created'])
                