import boto3
from botocore.config import Config
from time import sleep
import re
from constants import CONFIG_AWS_ACCESS_KEY_ID, CONFIF_AWS_SECRET_ACCESS_KEY, CONFIG_AWS_S3_OUTPUT_BUCKET_NAME, CONFIG_AWS_REGION_NAME


def read_text_file(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        text = file.read()
    return text
    
class TTS():
    def __init__(self):
        self.task_id = ''
        aws_session = boto3.session.Session(
            aws_access_key_id=CONFIG_AWS_ACCESS_KEY_ID,
            aws_secret_access_key=CONFIF_AWS_SECRET_ACCESS_KEY,
            region_name=CONFIG_AWS_REGION_NAME
        )

        self.polly = aws_session.client('polly')
        self.s3 = aws_session.client('s3')

    def TextToSpeech(self, payload: str, prefix: str):
        job = self.polly.start_speech_synthesis_task(
            Engine='generative',
            LanguageCode='en-US',
            OutputFormat='mp3',
            OutputS3BucketName=CONFIG_AWS_S3_OUTPUT_BUCKET_NAME,
            OutputS3KeyPrefix=prefix,
            Text=payload,
            TextType='ssml',
            VoiceId='Matthew'
        )
        task_id = job['SynthesisTask']['TaskId']

        
        print(f"task_id: {task_id} | {job['SynthesisTask']['TaskStatus']}")
        while job['SynthesisTask']['TaskStatus'] in ['scheduled', 'inProgress']:
            print(f"Status is: {job['SynthesisTask']['TaskStatus']}")
            sleep(2)
            job = self.polly_status(task_id)
            if job['SynthesisTask']['TaskStatus'] not in ['scheduled', 'inProgress']:
                break

        if job['SynthesisTask']['TaskStatus'] in ['failed']:
            print(str(job))
        else:
            print(f"task_id: {task_id} | {job['SynthesisTask']['TaskStatus']}")
            file_name=f"{prefix}.{task_id}.mp3"
            print(f"Downloading file: {file_name}")
            self.s3_download(file_name, f'output_{file_name}')
            downloaded_file = f'output_{file_name}'
        return downloaded_file

    def polly_status(self, task_id):
        response = self.polly.get_speech_synthesis_task(
            TaskId=task_id
        )
        return(response)

    def s3_download(self, file, destination):
        self.s3.download_file(CONFIG_AWS_S3_OUTPUT_BUCKET_NAME, file, destination)

def FixTextInput(payload):
    # payload = payload.replace('\n', '<break time="300ms"/>')
    payload = payload.replace('\n', ' ')
    payload = payload.replace(':', '<break time="200ms"/>')
    payload = payload.replace('- ', '<break time="200ms"/>')
    payload = payload.replace('—', '<break time="200ms"/>')
    payload = payload.replace('#', '')
    payload = payload.replace(r'’', "'")
    payload = payload.replace(r'”', "\"")
    payload = payload.replace('    ', '<break time="500ms"/>')
    payload = payload.replace('&', "and")
    payload = payload.replace('•	 ', '<break time="500ms"/>')
    payload = re.sub(r"([a-zA-Z]+\.)[0-9]{1,2}\ ", r"\1 ", payload) # remove references in text
    payload = re.sub(r'([F|f]igure [0-9]{1,2}.[0-9]{1,2} [\w\d\s]*\.)', r'\1 <break time="20s"/>', payload) # Add delay when a figure is mentioned
    #payload = payload.replace('..', '.')
    return f'<speak>{payload}</speak>'

def open_file(filepath):
    import subprocess, os, platform
    if platform.system() == 'Darwin':       # macOS
        subprocess.call(('open', filepath))
    elif platform.system() == 'Windows':    # Windows
        os.startfile(filepath)
    else:                                   # linux variants
        subprocess.call(('xdg-open', filepath))