#!/bin/bash

version="1.01.4"
welcome_file="/etc/update-motd.d/98-openvoid-welcome"

config_login=false
config_hostname=false
config_ip=false
inst_mysql=false
inst_build=false
inst_updates=false
inst_webserv=false
inst_apt_cache=false

function header {
        clear
        echo "##########################################"
        echo "# The Ubuntu config script V$version #"
        echo "##########################################"
        echo ""
        echo ""
}

function menu {
        header
        echo ">>> This needs to run as root <<<"
        echo ""
        echo "Please pick a task below:"
        echo "1) Password reset"
        echo "2) Hostname change"
#        echo "3) IP Static - faulty"
#       echo "4) IP DHCP"
        echo "5) Install Linux caching server"
        echo "6) Update/Upgrade system"
        echo "7) Install Web server"
        echo "8) Install compling software"
        echo "9) Install Mysql"
        echo "q) Quit,Apply and Reboot"
}

function config_pending {
        echo ""
        echo "Pending changes:"
        if [ $config_login = true ]; then
                echo "> Update password for user ($update_USERNAME)"
        fi
        if [ $config_hostname = true ]; then
                echo "> Update hostname to $new_hostname"
        fi
        if [ $config_ip = true ]; then
                echo "> Update device ($device)"
                echo ">> IP: $ip"
                echo ">> Subnet: $subnet"
                echo ">> Broadcast: $broadcast"
                echo ">> Gateway: $gateway"
                echo ">> DNS: $dns"
        fi
        if [ $inst_updates = true ]; then
                echo "> Install system updates"
        fi
        if [ $inst_build = true ]; then
                echo "> Install Compiling software"
        fi
        if [ $inst_webserv = true ]; then
                echo "> Install Web Server"
        fi
        if [ $inst_mysql = true ]; then
                echo "> Install Mysql"
        fi
        if [ $inst_apt_cache = true ]; then
                echo "> Install Linux APT cache"
        fi
        echo "__end of list__"
}

function config_do {
        if [ $config_login = true ]; then
                header
                echo ">  Setting new password"
                echo "$update_USERNAME:$update_PASSWORD" | chpasswd
        fi
        if [ $config_hostname = true ]; then
                header
                echo ">  Setting new Hostname"
                hostname "$new_hostname"
                echo "$new_hostname" > /etc/hostname
                cat /etc/hosts | sed s/"$old_hostname"/"$new_hostname"/ > /tmp/newhosts
                cat /tmp/newhosts > /etc/hosts
        fi
        if [ $inst_updates = true ]; then
                header
                echo ">  Updating system"
                apt-get update -y; apt-get upgrade -y;
        fi
        if [ $inst_build = true ]; then
                header
                echo ">  Installing compiling sofware"
                apt-get install make build-essential -y
        fi
        if [ $inst_webserv = true ]; then
                header
                echo ">  Installing Web server"
                apt-get install apache2 php libapache2-mod-php php-mysql php-gd libgmp-dev php-gmp php-curl -y
        fi
        if [ $inst_mysql = true ]; then
                header
                echo ">  Installing Mysql"
                apt-get install mysql-server libmysqld-dev -y
        fi
        if [ $inst_apt_cache = true ]; then
                header
                echo ">  Linuc APT cache"
                apt-get install apt-cacher apache2 -y
                header
                echo "Please open a new console and edit the following"
                echo ""
                echo "File : /etc/default/apt-cacher"
                echo "AUTOSTART=1"
                echo ""
                echo "File: /etc/apt-cacher/apt-cacher.conf"
                echo "allowed_hosts = *"
                echo "installer_files_regexp = ^(?:vmlinuz|linux|initrd\.gz|changelog|NEWS.Debian|[a-z]+\.tar\.gz(?:\.gpg)?|UBUNTU_RELEASE_NAMES\.tar\.gz(?:\.gpg)?|(?:Devel|EOL)?ReleaseAnnouncement(?:\.html)?|meta-release(?:-lts)?(?:-(?:development|proposed))?)$ "
                echo ""
                echo ""
                echo "please test http://<serverip>:3142/apt-cacher"
                pause
                task_done
        fi
        if [ $config_ip = true ]; then
                header
                echo ">  Setting new IP config"
                ifconfig $device $ip
                ifconfig $device netmask $subnet
                ifconfig $device broadcast $broadcast
                route add default gw $gateway $device
                echo "nameserver $dns" > /etc/resolv.conf
        fi
}

function task_done {
        echo "This config is now pending"
        echo "=> Please press any key to return <="
        read void
}

function update_login {
        header
        echo "Please provide username:"; echo -n "> "
        read update_USERNAME
        echo "Please provide new password:"; echo -n "> "
        read update_PASSWORD
        config_login=true
        echo "$update_USERNAME:$update_PASSWORD" | chpasswd
        header
        taks_done
}

function host_update {
        header
        old_hostname=$(cat /etc/hostname)
        echo "Please enter new Hostname for $old_hostname"; echo -n "> "
        read new_hostname
        config_hostname=true
        task_done
}

function network_static {
        header
        echo "Please select device to configure"
        ip -o link show | awk '{print $2,$9}'; echo -n "> "
        read device
        echo "Please enter IP of this host"; echo -n "> "
        read ip
        echo "Plesse enter Subnet (eg. 255.255.255.0)"; echo -n "> "
        read subnet
        echo "Please enter Broadcast"; echo -n "> "
        read broadcast
        echo "Please enter Default gateway"; echo -n "> "
        read gateway
        echo "Please enter DNS"; echo -n "> "
        read dns
        config_ip=true
        task_done
}

function network_dhcp {
        header
        echo "Please configure this by updating /etc/network/interfaces"
        echo "This should look something linke the below"
        echo "iface eth0 inet dhcp"
#        echo "Please select device to configure"
#        ip -o link show | awk '{echo $2,$9}'
#        read device
#       dhclient
        task_done
}

function install_updates {
        inst_updates=true
}

function install_webserv {
        install_updates
        inst_webserv=true
        task_done
}

function install_build {
        install_updates
        inst_build=true
        task_done
}

function install_mysql {
        install_updates
        inst_mysql=true
        task_done
}

function install_apt_cache {
        install_updates
        inst_apt_cache=true
        task_done
}

function motd_update {
        welcome_string="This box was setup by the Openvoid Ubuntu set script V $version"
        echo 'echo "' > welcome_file
        echo '$welcome_string' >> welcome_file
        echo '"' >> welcome_file
}

function loop {
        while true; do
                input=""
                clear
                menu
                config_pending
                echo ""
                echo -n "> "
                read input
                if [ "$input" = "q" ]; then
                        break
                elif [ "$input" = "1" ]; then
                        update_login
                elif [ "$input" = "2" ]; then
                        host_update
                elif [ "$input" = "3" ]; then
                        network_static
#               elif [ "$input" = "4" ]; then
#                       network_dhcp
                elif [ "$input" = "5" ]; then
                        install_apt_cache
                elif [ "$input" = "6" ]; then
                        install_updates
                elif [ "$input" = "7" ]; then
                        install_webserv
                elif [ "$input" = "8" ]; then
                        install_build
                elif [ "$input" = "9" ]; then
                        install_mysql
                fi
        done
}

loop
motd_update

config_do
header
echo 'System Rebooting...'
reboot


## PEAR
# wget http://pear.php.net/go-pear.phar
# php go-pear.phar

# sudo a2enmod rewrite
# sudo php5enmod mcrypt

#update rep
#https://askubuntu.com/questions/20414/find-and-replace-text-within-a-file-using-commands

# update ntpdate to crontab 