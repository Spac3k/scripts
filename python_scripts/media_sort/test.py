﻿import unittest
from helpers import hash
from helpers import read_file_date


buffer = pow(2, 18)
test_file = "internal_data/test_file.png"

class TestHashingOutput(unittest.TestCase):
    def test_md5_file_input(self):
        self.assertEqual(hash(test_file, buffer, "md5"), "d5ab67cd348ca47413315dc7c468aca1")

    def test_md5_string_input(self):
        string_input = "testing"
        self.assertEqual(hash(string_input, buffer, "md5", input_payload_is_file=False), "ae2b1fca515949e5d54fb22b8ed95575")

    def test_md5_int_input(self):
        int_input = 1234567890
        self.assertEqual(hash(int_input, buffer, "md5", input_payload_is_file=False), "e807f1fcf82d132f9bb018ca6738a19f")

    def test_sha256_file_input(self):
        self.assertEqual(hash(test_file, buffer, "sha256"), "0a8093c3df4b0f6aff79124ad793ee5df5769cedbb79e663cbfad8beeecfd8fd")

    def test_sha256_string_input(self):
        string_input = "testing"
        self.assertEqual(hash(string_input, buffer, "sha256", input_payload_is_file=False), "cf80cd8aed482d5d1527d7dc72fceff84e6326592848447d2dc0b0e87dfc9a90")

    def test_sha256_int_input(self):
        int_input = 1234567890
        self.assertEqual(hash(int_input, buffer, "sha256", input_payload_is_file=False), "c775e7b757ede630cd0aa1113bd102661ab38829ca52a6422ab782862f268646")

    def test_filedate(self):
        files = [
                ["internal_data/2012-08-02-767.test", "2012-08-02 00:00:00"],
                ["internal_data/2017-01-22-1747.test", "2017-01-22 00:00:00"],
                ["internal_data/20140113_145007.test", "2014-01-13 14:50:07"],
                ["internal_data/20150614 002349-1.test","2015-06-14 00:23:49"],
                ["internal_data/07012012140.test", "2012-01-07 00:00:00"],
                ["internal_data/IMG-20200209-Whatsapp.text", "2020-02-09 00:00:00"],
                ["internal_data/IMG_7239.JPG", "2024-06-07 01:19:26"],
                ["internal_data/vlcsnap-2024-06-06-23h25m20s189.png", "2024-06-06 23:25:20"],
                ["internal_data/Screenshot_20240331_181857_WhatsApp.png", "2024-03-31 18:18:57"],
                ["internal_data/VID-20231212-WA0002", "2023-12-12 00:00:00"]
            ]

        for file in files:
            filename, expected_date = file
            self.assertEqual(read_file_date(filename).strftime("%Y-%m-%d %H:%M:%S"), expected_date)

if __name__ == '__main__':
    unittest.main()
