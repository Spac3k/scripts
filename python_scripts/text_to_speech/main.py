from helper import TTS, FixTextInput, open_file, read_text_file
from constants import CONFIG_TEXT_FILE_TO_READ, OUTPUT_MP3_PREFIX

text_to_read = read_text_file(CONFIG_TEXT_FILE_TO_READ)
speak = TTS()
speak_this = FixTextInput(text_to_read)
print(speak_this)
file_name = speak.TextToSpeech(speak_this, OUTPUT_MP3_PREFIX)
open_file(file_name)