## Description


## Status 
 - [x] Concept
 - [_] Experimental
 - [_] Integrated/Using

## Disclaimer
This code it open source for you to read and learn from. You run it at your own risk.

This script wil shutdown before loadsheading hits. it can read the time from home assistant or "Eskom Se Push"
The first run will create the template "config.ini" file.

## How to use
- pip install --upgrade pip
- pip install -r requirements.txt
- python3 main.py


need to test and update this section..



## auto start in windows
$action = New-ScheduledTaskAction -Execute "C:\Program Files\Python\Python38\python.exe" -Argument "C:\Scripts\mypScript.py"
$trigger = New-ScheduledTaskTrigger -Daily -At 5:00am
$trigger.StartBoundary = [DateTime]::Parse($trigger.StartBoundary).ToLocalTime().ToString("s")
$settings = New-ScheduledTaskSettingsSet -ExecutionTimeLimit 0

Register-ScheduledTask -Action $action -Trigger $trigger -Settings $settings -TaskName "Full Computer Backup" -Description "Backs up computer"