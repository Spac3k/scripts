import os, configparser
import platform
import schedule
from datetime import datetime
import http.client, urllib
import json
import time

from hass import Auth as HassAuth

config_file_name = "config.ini"
default_config = {
        'settings': {
            'time_source':'esp', # esp, hass
        },
        'esp': {
            'str_sepush_token':'token',
            'bool_sepush_test': True,
           'area': 'city-zone_num-area_name'
        },
        'pushover': {
            'str_pushover_app_token':'token',
            'str_pushover_user_key':'key',
        },
        'hass' : {
            'uri':'http://example.com/api',
            'token':'secret_access_token'
        }
    }
config = configparser.ConfigParser()
OS_type = platform.system().lower()

def load_settings():
    global config
    try:
        test_file = open(config_file_name, "r") # this triggers the FileNotFoundError
        test_file.close()
        config.read(config_file_name)
        print(f'Config file read from : {config_file_name}')
    except FileNotFoundError:
        with open(config_file_name, "w") as configfile_handler: 
            print(f'default_config len: {len(default_config)}')
            config.read_dict(default_config)
            config.write(configfile_handler)
        print('Created config file')
        exit()

def time_delta_seconds(datetime_loadsheading):

    datetime_time = datetime.now()
    print("time_delta_seconds now: {}".format(str(datetime_time)))

    # get difference in seconds
    delta = datetime_loadsheading - datetime_time
    seconds = delta.seconds
    print("time_delta_seconds: {}".format(seconds))
    return seconds

def get_loadsheading_time():
    if 'esp' in config['settings']['time_source']:
        return get_loadsheading_time_from_esp()
    elif 'hass' in config['settings']['time_source']:
        return get_loadsheading_time_from_hass()

def get_loadsheading_time_from_hass():
    hass = HassAuth(config['hass']['uri'], config['hass']['token'])
    
    resp = hass.request("get", "states/input_datetime.loadsheading_start")
    resp_dict = resp.json()
    time_str = resp_dict['state'] #'state': '2023-09-01 02:00:00'
    datetime_loadsheading = datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S")
    return datetime_loadsheading

def get_loadsheading_time_from_esp():
    print("in get_loadsheading")
    conn = http.client.HTTPSConnection("developer.sepush.co.za")
    payload = ''
    headers = {"token":config['esp']['str_sepush_token']}
    if config['esp']['bool_sepush_test'] == True:
        print("Test mode active")
        testing = "&test=current"
    else:
        testing = ""
    conn.request("GET", "/business/2.0/area?id={area_id}".format(area_id=config['esp']['area'],test=testing), payload, headers)
    res = conn.getresponse()
    data = res.read()
    dict_data = json.loads(data.decode("utf-8"))
    if 'error' in dict_data.keys():
        print(dict_data)

    str_time = dict_data["events"][0]["start"]
    # str_time = "2022-12-20T00:00:00+02:00" # testing
    print("get_loadsheading_time: {}".format(str_time))
    datetime_loadsheading = datetime.strptime(str_time, "%Y-%m-%dT%H:%M:%S+02:00")
    return datetime_loadsheading

def send_notification(str_add):
    conn = http.client.HTTPSConnection("api.pushover.net:443")
    conn.request("POST", "/1/messages.json",
      urllib.parse.urlencode({
        "token": config['pushover']['str_pushover_app_token'],
        "user": config['pushover']['str_pushover_user_key'],
        "message": "Midgard shutdown {}".format(str_add),
      }), { "Content-type": "application/x-www-form-urlencoded" })
    print(conn.getresponse())

def shutdown(timeout_seconds):
    '''
    It automate shutdown by scheduling the
    time for computer to shutdown
    '''

    print("shutdown: {}".format(timeout_seconds))
    if OS_type == 'linux':
        shutdown_str = 'shutdown -t '+ str(timeout_seconds)
    elif OS_type == 'windows':
        shutdown_str = 'shutdown /s /t ' + str(timeout_seconds)
    else:
        print('Sorry this feature is not available in ', OS_type)
        return

    os.system(shutdown_str)
    send_notification("set for {} seconds ({} mins)".format(timeout_seconds, timeout_seconds/60))
    print("Stopping the script via Ctrl+C will auto cancel the shutdown")

def cancel_shutdown():
    print("cancel_shutdown")
    if OS_type == 'linux':
        cancel_str = 'shutdown -c'
    elif OS_type == 'windows':
        cancel_str = 'shutdown /a'
    else:
        return
    os.system(cancel_str)
    send_notification("cancelled")

def check_loasheading():
    datetime_loadsheading = get_loadsheading_time()
    loadsheading_seconds_away = time_delta_seconds(datetime_loadsheading)

    loadsheading_mins_away = loadsheading_seconds_away/60
    if loadsheading_mins_away < 60 and loadsheading_mins_away > 0:
        print("check_loasheading: Shutdown")
        shutdown(loadsheading_seconds_away - 120)
    else:
        print("check_loasheading: No action")

def main():
    print("main: Start")
    load_settings()
    check_loasheading()
    schedule.every().hour.do(check_loasheading)
    try:
        while True:
            schedule.run_pending()
            time.sleep(60*10) # 10 minutes sleep as the run_pending take up cpu cycles.

    except KeyboardInterrupt:
        print("Stopping")
        cancel_shutdown()

if __name__ == '__main__':
    main()
    print("End")
