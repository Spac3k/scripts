import configparser
import logging
import csv
import traceback, sys
import os
import hashlib
from decimal import Decimal, ROUND_HALF_UP


### Programming config  ###
configuration_file = "budget.ini"
log_file = "budget.log"
hash_lookup_cat_name = "hash_lookup"
cat_name_budget = "cat_budget"
cat_cache = {}
cat_budget_cache = None
cat_hash_lookup = {}
config_cache = {}
detect_month_start = True # this is based on the highest income value

# create logger
log = logging.getLogger('budget calc')
log.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler(log_file)
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
log.addHandler(fh)
log.addHandler(ch)

def create_config():
    ''' create the default config '''
    name = "create_config"
    global config_cache, cat_cache
    log.debug("{} - setting default config".format(name))
    config = configparser.ConfigParser(allow_no_value=True)
    config.add_section('settings')
    config['settings'] = { 'once_off_cat': 'hash_cats.csv',
               'configured': True}
    config[cat_name_budget] = {}
    config[hash_lookup_cat_name] = {}
    cats = ['home','mobile','rates/taxes','internet','entertainment','fuel', 'savings', 'banking fees', 'income']
    for cat in cats:
        config.add_section(cat)
        
    with open(configuration_file, 'w') as configfile:
        config.write(configfile)
        configfile.close()
        
    config_cache = config['settings']
    
    load_config(False)
    # log.debug(config_cache)

def load_config(base_config=True):
    ''' loads in the configuration for the program '''
    name = "load_config"
    global cat_cache, config_cache, cat_budget_cache
    try:
        if base_config:
            log.debug("{} - loading base config".format(name))
            try:
                config = configparser.ConfigParser(allow_no_value=True)
                config.read(configuration_file)
                # log.debug(config.sections())
                standard_config = config['settings']
                is_configured = standard_config.get('configured', False)
                config_cache = standard_config
                # print (config_cache)
            except KeyError:
                log.debug("{} - base_config missing - creating".format(name))
                create_config()
            except:
                log.exception("{} - exception triggered".format(name))
        else:
            log.debug("{} - loading cat config".format(name))
            config = configparser.ConfigParser(allow_no_value=True)
            config.read(configuration_file)
            cat_cache = {}
            cat_budget = None
            catagories = config.sections()
            for catagory in catagories:
                if catagory == "settings":
                    continue
                elif catagory == cat_name_budget:
                    temp = {}
                    for (key, value) in config.items(catagory):
                        temp[key] = float(value)
                    #print(temp)
                    cat_budget_cache = temp
                    log.debug("{} - cat_budget_cache loaded {}".format(name,cat_budget_cache))
                elif catagory == hash_lookup_cat_name:
                    log.debug("{} - hash_lookup".format(name))
                    # temp_list{}
                    loop = 0
                    for (key, value) in config.items(catagory):
                        loop += 1
                        # log.debug("hash_lookup - {} {}".format(key, value))
                        # key.lower())
                        cat_hash_lookup[key] = value
                    log.debug("{} - hash_lookup loaded {} keys".format(name,len(cat_hash_lookup)))
                else:
                    temp_list = []
                    # log.debug(config[catagory])
                    for item in config[catagory]:
                        # log.debug(item)
                        temp_list.append(item.lower())
                    cat_cache[catagory.lower()] = temp_list
            prep_cat_budget()
            # log.debug(cat_cache)
    except Exception:
        traceback.print_exc(file=sys.stdout)


def update_config(section, variable, value=None, auto_reload=True):
    ''' creates a new string to match to a catagory '''
    name = "update_config"
    global cat_cache, config_cache
    log.debug("{} - update_config {}-{}-{}".format(name,section,variable,value))
    section = section.lower()
    variable = variable.lower()
    # check if it already mtaches find_cat(variable)
    config = configparser.ConfigParser(allow_no_value=True)
    config.read(configuration_file)
    # 'BatchMode' in topsecret
    try:
        config.add_section(section)
        log.debug("{} - created new section: {}".format(name,section))
    except:
        pass # already exist
        
    if section in hash_lookup_cat_name:
        try:            
            config.add_section(value)
            log.debug("{} - created new section: {}".format(name,section))
        except:
            pass # already exist
        
    try:
        if value is not None:
            config.set(section,variable,value)
            log.debug("{} - added new variable with value.".format(name))
        else:
            config.set(section,variable)            
            log.debug("{} - added new variable without value.".format(name))
    except:
        log.exception("{} - exception triggered".format(name))
        
    with open(configuration_file, 'w') as configfile:
        config.write(configfile)
        configfile.close()
        
    if auto_reload:
        load_config(False) # reload cat config

def remove_config(section, variable):
    name = "remove_config"
    config = configparser.ConfigParser(allow_no_value=True)
    config.read(configuration_file)
    config.remove_option(section, variable)
    log.debug("{} - removed Section: {}  variable: {}".format(name,section,variable))
    with open(configuration_file, 'w') as configfile:
        config.write(configfile)
        configfile.close()

def add_cat(dict):
    ''' requests the user input on the catagory for the string '''
    name = "add_cat"
    string = dict["desc"]
    once_off_lookup = False
    global cat_cache
    match_string = ""
    screen("Price: {}".format(dict["amount"]).ljust(25," ") + "Statement: {}".format(string))
    while "!" not in match_string and (match_string not in string or match_string == ""): # truns out that "" exist in any string, also "" is for blank input().
        match_string = input("Please provide a string to match the statement: ").lower()
        match_string = match_string.strip()
        match_string = match_string.replace("#", "")
        match_string = match_string.replace("[", "")
        match_string = match_string.replace("]", "")
        match_string = match_string.replace("=", "")
        # if ",." in match_string: # used to skip the line, or not write it to file - WIP
        #     continue
    if "!" in match_string:
        log.debug("{} - Noted that this is a once off selection".format(name))
        screen("Noted that this is a once off selection")
        once_off_lookup = True
        
    all_cats = list_cats()
    # log.debug(all_cats)
    options = {}
    cat_num = 0
    for cat in all_cats:
        cat_num += 1
        options[cat_num] = cat
        screen("{}) : {}".format(cat_num, cat))
    input_cat = input("Please select which Catagory this should belong to, or type a new name: ").lower()
    
    
    try:
        cat_num = int(input_cat)
        log.debug("{} - submit : {} with {}".format(name, options[cat_num], match_string))
        if once_off_lookup:
            update_config(hash_lookup_cat_name, hash(dict), options[cat_num])
        else:
            update_config(options[cat_num], match_string)
    except ValueError:
        # log.debug("new cat")        match_string.strip()
        input_cat = input_cat.strip()
        input_cat = input_cat.replace("#", "")
        input_cat = input_cat.replace("[", "")
        input_cat = input_cat.replace("]", "")
        input_cat = input_cat.replace("=", "")
        if once_off_lookup:
            update_config(hash_lookup_cat_name, hash(dict), input_cat.strip())
        else:
            update_config(input_cat.strip(), match_string)
    except:
        log.exception("{} - exception triggered".format(name))


def find_cat(string,string_hash,detail=False):
    ''' finds the catagory for the string in question '''
    name = "find_cat"
    global cat_cache
    string = string.lower()
    
    if string_hash in cat_hash_lookup:
        if detail:
            return {"found":True,"cat":cat_hash_lookup[string_hash],"string":hash_lookup_cat_name}
        else:
            return cat_hash_lookup[string_hash]
            
    for cat in cat_cache:
        #log.debug(cat)
        for item in cat_cache[cat]:
            #log.debug(item)
            if item in string:
                #log.debug("item")
                #log.debug(item)
                if detail:
                    return {"found":True,"cat":cat,"string":item}
                else:
                    return cat
    
    if detail:
        return {"found":False,"cat":None,"string":None}
    else:
        return None

def list_cats():
    ''' finds the catagory for the string in question '''
    name = "list_cats"
    global cat_cache
    cats = []
    for cat in cat_cache:
        cats.append(cat.lower())
    cats.sort()
    return cats

def prep_cat_budget():
    ''' load the planned spend on each catagory and create if required '''
    name = "prep_cat_budget"
    global cat_budget_cache

    if cat_budget_cache is None:
        update_config(cat_name_budget,"")
    else:
        for catagory in cat_cache:
            if catagory in cat_budget_cache:
                continue
            else:
                log.debug("{} - creating budget for {}".format(name, catagory))
                update_config(cat_name_budget,catagory,0, auto_reload=False)
        
    for catagory in cat_budget_cache:
        try:
            float(cat_budget_cache[catagory])
            log.debug("{} - float test pass for {} ({})".format(name, catagory, cat_budget_cache[catagory]))
        except ValueError:
            cat_budget_cache[catagory] = 0
            update_config(cat_name_budget,catagory,0, auto_reload=False)
            log.debug("{} - ValueError - re-write value for {} to zero".format(name, cat_budget_cache[catagory]))
        except:
            log.exception("{} - exception triggered".format(name))


def find_statement():
    '''Try to find the statement file '''
    name = "find_statement"
    possible_files = []
    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    for f in files:
        if ".csv" in f:
            possible_files.append(f)
    if len(possible_files) == 1:
        statement = possible_files[0]
    else:
        options = {}
        count = 0
        log.debug("{} - Please select the file you wish to open:".format(name))
        for file in possible_files:
            count += 1
            options[count] = file
            print ("{}) - {}".format(count,file))
        valid_option = False
        while not valid_option:
            choice = input("> ")
            try:
                test1 = int(choice)
                if test1 <= count and test1 > 0:
                    statement = options[test1]
                    valid_option = True
            except:
                log.debug("{} - try again".format(name))
                break
    return(statement)

    
    
def open_statement():
    ''' opens the statement file and tries to read the data '''
    name = "open_statement"
    global config_cache
    # statement_file = config_cache["statement_file"]
    statement_file = find_statement()
    data = csv.reader(open(statement_file, newline='\n'), delimiter=',')
    header_found = False
    statement = []
    row_len = 0 
    temp_statement = []
    income_amount = 0
    # log.debug("stanitizing statement")
    for row in data:
        # log.debug(str(" // ".join(row).lower()))
        if ("date" not in str("    ".join(row).lower())) and not header_found:
            log.debug("{} - Searching for header".format(name))
            continue
        elif row_len == 0 and not header_found and "date" in str("    ".join(row).lower()):
            log.debug("{} - Header found".format(name))
            row_len = len(row)
            header_found = True
            column = 0
            for item in row:
                item = item.lower()
                if "date" in item:
                    csv_date  = column
                    # log.debug("date is located at column {}".format(csv_date))
                if "amount" in item:
                    csv_amount  = column
                    # log.debug("amount is located at column {}".format(csv_amount))
                if "description" in item:
                    csv_desc  = column
                    # log.debug("description is located at column {}".format(csv_desc))
                column += 1            
        elif len(row) == row_len: # used to check the amount of colums in the line, skip data at the end of statement
            # log.debug("data added")
            if detect_month_start:
                if float(row[csv_amount]) > income_amount:
                    income_amount = float(row[csv_amount])
            temp_statement.append(row)

        else:
            pass # log.debug("discard unknown data")
    found_start = False
    for row in temp_statement:
        if detect_month_start:
            if float(row[csv_amount]) == income_amount and not found_start:
                found_start = True
                log.debug("{} - Found day of income: {}".format(name, row[csv_date]))
            
            if found_start:
                temp_for_hash = {"date":row[csv_date],"amount":row[csv_amount],"desc":row[csv_desc].lower()}
                cat = find_cat(row[csv_desc].lower(),hash(temp_for_hash))
                statement.append({"date":row[csv_date],"amount":row[csv_amount],"desc":row[csv_desc].lower(),"cat":cat})
            else:
                log.debug("{} - Searching for start of month, day of income".format(name))
        else:
            temp_for_hash = {"date":row[csv_date],"amount":row[csv_amount],"desc":row[csv_desc].lower()}
            cat = find_cat(row[csv_desc].lower(),hash(temp_for_hash))
            statement.append({"date":row[csv_date],"amount":row[csv_amount],"desc":row[csv_desc].lower(),"cat":cat})
    
    log.debug("{} - Loaded {} entries.".format(name,len(statement)))
    # fail if the required fields are not found
    return statement

def hash(dict):
    name = "hash"
    string = str(dict["date"]).strip() + str(dict["amount"]).strip() + str(dict["desc"]).lower().strip()
    encoded = string.encode()
    var = hashlib.md5(encoded).hexdigest()
    return var

def screen(string):
    print(string)

def num_test(value):
    ''' test for any annomalys in numbers, like long floats '''
    name = "num_test"
    # log.debug("{} - Checking number: {}".format(name, value))
    string_float = str(float(value))
    dec_count = len(str(value).split(".")[1])
    if dec_count > 2:
        log.debug("{} - the nubmer is {} long and has {} decimal points".format(name,  string_float,  dec_count))
        #raise decimalcount
    return value

def main():
    ''' everything runs from here '''
    name = "main"
    load_config()
    load_config(False) # cat config
    needs_loading = True
    statement = open_statement()
    while needs_loading and len(statement) > 0:
        log.debug("{} - Looping over statement".format(name))
        needs_loading = False
        for row in statement:
            
            log.debug("{} - Catagory: {}".format(name,row["cat"]).ljust(50," ") + "- Line in statement: {}".format(row["desc"]))
            num_test(Decimal(row["amount"]))
            if row["cat"] == None:
                log.debug("{} - need to update cat.".format(name))
                add_cat(row)
                needs_loading = True
                break
                log.debug("{} - After break".format(name))

        log.debug("{} - needs_loading: {}".format(name,str(needs_loading)))
        if needs_loading:
            statement = open_statement()
            #log.debug("for loop end")
        #log.debug("while loop end")
        
    cat_sum = {}
    temp_income = []
    temp_expenses = []
    screen("Calulating Catagory sums")
    for row in statement:
        try:
            cat_sum[row["cat"]] += num_test(Decimal(row["amount"].strip()))
        except KeyError:
            cat_sum[row["cat"]] = num_test(Decimal(row["amount"].strip()))
        except:
            log.exception("{} - exception triggered".format(name))
            
    log.debug("{} - cat_sum: {}".format(name, cat_sum))
    screen("Sorting data")
    # cat_sum_sorted = sorted(cat_budget_cache, key=cat_budget_cache.__getitem__)
    cat_sum_sorted = sorted(cat_budget_cache, key=lambda x: cat_budget_cache[x], reverse=True)

    log.debug("{} - cat_sum_sorted: {}".format(name,str(cat_sum_sorted)))
    for catagory in cat_sum_sorted:
        try:
            if cat_sum[catagory] > 0:
                temp_income.append(catagory)
            else:
                temp_expenses.append(catagory)
        except KeyError:
            continue
        except:
            log.exception("{} - exception triggered".format(name))
        
    cat_offset = 30
    budget_offset = 10
    # log.debug("{} - ".format(name) + "Catagory".ljust(cat_offset, ' ') + "Budget".ljust(budget_offset, ' ') + "Usage".ljust(budget_offset, ' ') + "Left")
    try:
        cents = Decimal('0.01')
        screen("==========================================")
        screen("================  Income  ================")
        screen("==========================================")
        screen("Catagory".ljust(cat_offset, ' ') + "Budget".ljust(budget_offset, ' ') + "Usage".ljust(budget_offset, ' ') + "Diffrince")
        income = 0
        for catagory in temp_income:
                cat_value = num_test(Decimal(cat_sum[catagory]).quantize(cents, ROUND_HALF_UP))
                budget_value = Decimal(cat_budget_cache[catagory]).quantize(cents, ROUND_HALF_UP)
                budget_left = Decimal((budget_value) - cat_value).quantize(cents, ROUND_HALF_UP)
                string_cat = "{}".format(catagory).ljust(cat_offset, ' ')
                string_budget = "{}".format(budget_value).ljust(budget_offset, ' ')
                string_used = "{}".format(cat_value).ljust(budget_offset, ' ')
                string_left = "{}".format(budget_left).ljust(budget_offset, ' ')
                #log.debug("{} - ".format(name) + string_cat + string_budget + string_used + string_left )
                screen(string_cat + string_budget + string_used + string_left )
                income += cat_value
        screen("")
        screen("==========================================")
        screen("================ Expense  ================")
        screen("==========================================")
        screen("Catagory".ljust(cat_offset, ' ') + "Budget".ljust(budget_offset, ' ') + "Usage".ljust(budget_offset, ' ') + "Left")
        used = 0
        to_use = 0
        over_used = 0
        for catagory in temp_expenses:
                cat_value = num_test(Decimal(cat_sum[catagory]).quantize(cents, ROUND_HALF_UP))
                budget_value = Decimal(cat_budget_cache[catagory]).quantize(cents, ROUND_HALF_UP)
                budget_left = Decimal((budget_value) + cat_value).quantize(cents, ROUND_HALF_UP)
                string_cat = "{}".format(catagory).ljust(cat_offset, ' ')
                string_budget = "{}".format(budget_value).ljust(budget_offset, ' ')
                string_used = "{}".format(cat_value).ljust(budget_offset, ' ')
                string_left = "{}".format(budget_left).ljust(budget_offset, ' ')
                #log.debug("{} - ".format(name) + string_cat + string_budget + string_used + string_left )
                screen(string_cat + string_budget + string_used + string_left )
                if budget_left > 0:
                    to_use += budget_left
                else:
                    over_used += budget_left
                    used += cat_value
    except ValueError as e:
        log.exception("{} - except ValueError triggered".format(name))
        log.debug("{} - ValueError - catagory: {}, cat_value: {}, budget_value: {}".format(name,catagory,cat_value,budget_value))
    except:
        log.exception("{} - exception triggered".format(name))
    
    screen("\n\nYou have overspent by {} and still need to spend {}".format(str(over_used)[1:], to_use))
    screen("Your total income was {}, which leaves you with {} and {} at the end of the month.".format(income,income + used, income + used - to_use))
    # for cat in cat_sum:
    #     # log.debug("cat [{}] : {}".format(cat))
    
if __name__ == "__main__":
    main()