import requests

class Auth:
    """Class to make authenticated requests."""

    def __init__(self, host: str, access_token: str):
        """Initialize the auth."""
        self.host = host
        self.access_token = access_token

    def request(self, method: str, path: str, **kwargs) -> requests.Response:
        """Make a request."""
        headers = kwargs.get("headers")

        if headers is None:
            headers = {}
        else:
            headers = dict(headers)

        headers["Authorization"] = f'Bearer {self.access_token}'
        headers["Content-Type"] = "application/json"
        feedback = requests.request(
            method, f"{self.host}/{path}", **kwargs, headers=headers,
        )
        return feedback