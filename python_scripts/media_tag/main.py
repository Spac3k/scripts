from constants import INPUT_IMAGE_FILENAME, OUTPUT_IMAGE_FILENAME
from nudenet import NudeDetector
import xml.etree.ElementTree as ET 
import datetime


def XmlGetDate(xmlfile): 
    
    # https://medium.com/@gullevek/reading-xmp-sidecar-files-with-python-and-libxmp-a4c52e9955bb
    # https://github.com/python-xmp-toolkit/python-xmp-toolkit
  
    # create element tree object 
    tree = ET.parse(xmlfile) 
  
    # get root element 
    root = tree.getroot() 
  
    date = datetime.datetime.strptime(root[0][0].attrib['{http://ns.adobe.com/exif/1.0/}DateTimeOriginal'], "%Y:%m:%d %H:%M:%S.%f")
    return date 

# detector.censor( test_file ) # output a censored version of the im

if __name__ == "__main__":
    # NSFW detector
    detector = NudeDetector(inference_resolution=640)
    print(detector.detect(INPUT_IMAGE_FILENAME))
    print(XmlGetDate(f"{INPUT_IMAGE_FILENAME}.xmp"))