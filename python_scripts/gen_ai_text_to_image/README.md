## Description


## Status 
 - [_] Concept
 - [X] Experimental
 - [_] Integrated/Using

## Disclaimer
This code it open source for you to read and learn from. You run it at your own risk.

## How to use
- sudo apt-get install python3-pip
- python3 -m pip install --upgrade pip
- python3 -m pip install -r requirements.txt
- python3 main.py